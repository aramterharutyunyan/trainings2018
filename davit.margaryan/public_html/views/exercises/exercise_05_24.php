<?php
    $height = isset($_POST['height']) ? $_POST['height'] : '';
    echo '<section>';
    echo     '<div class="article">';
    if ($height != '' && ($height <= 0 || $height > 19)) {
        echo     '<div class="alert alert-danger">Error 1: Number of dimonds is out of maxY.</div>';
    } elseif ($height != '' && ($height & 1) == 0) {
        echo     '<div class="alert alert-danger">Error 2: Number of dimonds must be odd.</div>';
    } else {
        echo     '<div>';
        echo         '<pre>';
                      echo $this->get('diamond');
        echo         '</pre>';
        echo     '</div>';
    }
    echo         '<form method="post" action="" class="my-forms">';
    echo             '<label>Height of diamond*: </label>';
    echo             '<input type="text" name="height" placeholder="odd number (1 - 19)" />';
    echo             '<input type="submit" name="print" value="print">';
    echo         '</form>';
    echo     '</div>';
    echo '</section>';
?>
