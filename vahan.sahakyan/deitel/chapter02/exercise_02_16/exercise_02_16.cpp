#include <iostream>

int
main()
{
    int number1, number2;		 /// Initialization

    std::cout << "First number: ";	 /// Promts the user to input a value for number1
    std::cin >> number1;		 /// Saves the input value into number1

    std::cout << "Second number: ";	 /// Promts the user to input a value for number2
    std::cin >> number2;		 /// Saves the input value into the number2

    std::cout << "Sum: " << number1 + number2 << std::endl;		 /// Outputs the Sum
    std::cout << "Difference: " << number1 - number2 << std::endl;	 /// Outputs the Difference
    std::cout << "Product: " << number1 * number2 << std::endl;		 /// Outputs the Product

    if (0 == number2) {
        std::cout << "Error 1: Cannot divide by 0" << std::endl;
        return 1;
    } /// Outputs an "Error Message", if the number2 is equal to 0

    std::cout << "Quotient: " << number1 / number2 << std::endl;	/// Outputs the Quotient 

    return 0;
}
