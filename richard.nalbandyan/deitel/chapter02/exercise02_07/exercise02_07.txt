Discuss the meaning of each of the following objects: 
a. std::cin This object is the standard input from the iostream that takes a value from the user and stores it in a memory slot. 
b. std::cout This object is the standard output from the iostream that takes the value of a specified variable (std::cout << <variable name>) or some sort of text (std::cout << "<text>") and prints it on the screen.