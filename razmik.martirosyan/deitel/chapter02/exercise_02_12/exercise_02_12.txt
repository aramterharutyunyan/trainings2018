a) cout << x; // 2
b) cout << x + x;  // 4
c) cout << "x = ";   //  x =
d) cout << " x = " < x; //  nothing
e) cout << x + y << " = " << y + x // 5 = 5
f) z = x + y;  //  nothing
g) cin >> x >> y;  //  nothing
h) //cout << " x + y = " << x + y;  //  nothing
i) cout << "\n"; //  empty line

